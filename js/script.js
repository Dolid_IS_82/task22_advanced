let rows = document.getElementById("rows");
let cols = document.getElementById("cols");
let runButton = document.getElementById("run");
let tableContainer = document.getElementById("tableContainer");

function buildTable() {

    if (isNaN(rows.value) || isNaN(cols.value)) {
        alert("Incorrect row or column value");
        return;
    }

    let table = "<table id=table>";
    for (let i = 1; i <= Number(rows.value); i++) {
        table += "<tr>";

        for (let j = 1; j <= Number(cols.value); j++) {
            table += "<td>" + i + j + "</td>";
        }

        table += "</tr>"
    }

    table += "</table>";

    tableContainer.innerHTML = table;
}

var tablePaint;
runButton.onclick = function() {
    buildTable();
    tablePaint = document.getElementsByTagName('table')[0];
    tablePaint.addEventListener('click', changeStyle, false);
}

var changeStyle = function(e) {
    if (e.target.tagName.toLowerCase() != "td") {
        return;
    }

    if (e.target.style.backgroundColor == 'rgb(125, 125, 125)') {
        e.target.style.backgroundColor = 'rgb(255, 255, 255)';
    } else {
        e.target.style.backgroundColor = 'rgb(125, 125, 125)';
    }
};